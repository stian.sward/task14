﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace task14.Migrations
{
    public partial class InitialDB : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "People",
                columns: table => new
                {
                    Pid = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    FirstName = table.Column<string>(nullable: true),
                    LastName = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_People", x => x.Pid);
                });

            migrationBuilder.InsertData(
                table: "People",
                columns: new[] { "Pid", "FirstName", "LastName" },
                values: new object[,]
                {
                    { 1, "Ola", "Nordmann" },
                    { 2, "Kari", "Nordmann" },
                    { 3, "Navn", "Navnesen" },
                    { 4, "Placeholder", "Placeholderson" }
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "People");
        }
    }
}
