﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace task14.Models
{
    public class PeopleDbContext : DbContext
    {
        public DbSet<Person> People { get; set; }
        public PeopleDbContext(DbContextOptions options) : base(options) 
        { 
            Database.EnsureCreated();
        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            builder.Entity<Person>().HasData(new List<Person>
            {
                new Person { Pid = 1, FirstName = "Ola", LastName = "Nordmann" },
                new Person { Pid = 2, FirstName = "Kari", LastName = "Nordmann" },
                new Person { Pid = 3, FirstName = "Navn", LastName = "Navnesen" },
                new Person { Pid = 4, FirstName = "Placeholder", LastName = "Placeholderson" }
            });
        }
    }
}
